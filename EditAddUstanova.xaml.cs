﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using SF53_2018_POP2019.Model;
using SF53_2018_POP2019.Model.Dao;

namespace SF53_2018_POP2019
{
    /// <summary>
    /// Interaction logic for EditAddUstanova.xaml
    /// </summary>
    public partial class EditAddUstanova : Window
    {
        UstanovaDAO udao = new UstanovaDAO();
        public EditAddUstanova()
        {
            InitializeComponent();
            
            editadd_id.Text = udao.newId().ToString();
        }
        
        public EditAddUstanova(Ustanova ust)
        {
            InitializeComponent();
            editadd_id.Text = ust._Id.ToString();
            editadd_naziv.Text = ust._Naziv;
            editadd_adresa.Text = ust._Adresa;
            editadd_status.IsChecked = false;
            if (ust._Status != "Deleted")
            {
                editadd_status.IsChecked = true;
            }
        }

        private void save(object sender, RoutedEventArgs e)
        {
            int id = int.Parse(editadd_id.Text);
            int idsledeci = udao.newId();

            //Konstruisem ustanovu
            Ustanova ust = new Ustanova();
            ust._Id = int.Parse(editadd_id.Text);
            ust._Naziv = editadd_naziv.Text;
            ust._Adresa = editadd_adresa.Text;
            ust._Status = "Deleted";
            if ((Boolean)editadd_status.IsChecked)
            {
                ust._Status = "Active";
            }

            Boolean xd = false;
            if (id == idsledeci)
            {
                xd = udao.newUstanova(ust);
            }
            else
            {
                xd = udao.editUstanova(ust);
            }
            if (xd)
            {
                this.Close();
                UstanoveWindow uw = new UstanoveWindow();
                uw.Show();
            }
            else
            {
                editadd_errorMsg.Content = "Desila se greska, proverite unose!";
            }
        }

        private void cancel(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
