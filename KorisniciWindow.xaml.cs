﻿using SF53_2018_POP2019.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using SF53_2018_POP2019.Util;
using SF53_2018_POP2019.Model;
using SF53_2018_POP2019.Model.Dao;
using System.Data;

namespace SF53_2018_POP2019
{
    /// <summary>
    /// Interaction logic for KorisniciWindow.xaml
    /// </summary>
    public partial class KorisniciWindow : Window
    {
        ICollectionView view;
        public KorisniciWindow()
        {
            UsersDAO dao = new UsersDAO();
            InitializeComponent();
            initializeView(dao);

        }

        public void initializeView(UsersDAO dao)
        {
            List<User> listaKorisnika = dao.selectUsers();
            ucitajUTabelu(listaKorisnika);
            
        }

        private void dodajUsera(object sender, RoutedEventArgs e)
        {
            userWindow uwindow = new userWindow();
            uwindow.Show();
        }
        public void ucitajUTabelu(List<User> listaKorisnika)
        {
            AdminDAO admindao = new AdminDAO();
            Administrator admin = admindao.adminFromID(1);
            view = CollectionViewSource.GetDefaultView(listaKorisnika);
            Console.WriteLine(listaKorisnika);
            tableKorisnici.ItemsSource = view;
            tableKorisnici.CanUserAddRows = false;
        }

        private void ObrisiKorisnika(object sender, RoutedEventArgs e)
        {
            if (tableKorisnici.SelectedItem != null)
            {
                User row = (User)tableKorisnici.SelectedItems[0];
                UsersDAO userdao = new UsersDAO();
                Boolean obrisano = userdao.deleteUser(row._Id);
                if (obrisano)
                {
                    KorisniciWindow kw = new KorisniciWindow();
                    kw.Show();
                    this.Close();
                }
                else
                {
                    prikaziError("Nije bilo moguce obrisati.");
                }
            }
            else
            {
                prikaziError("Odaberite korisnika kojeg zelite da obrisete");
            }
        }
        public void prikaziError(String error)
        {
            errorMsgLabel.Visibility = Visibility.Visible;
            errorMsgLabel.Content = error;
        }

        private void izmeniKorisnika(object sender, RoutedEventArgs e)
        {
            if (tableKorisnici.SelectedItem != null)
            {
                User row = (User)tableKorisnici.SelectedItems[0];
                if (row.Tip == TypeUser.Admin)
                {
                    AdminDAO admindao = new AdminDAO();
                    Console.WriteLine((int)row._Id);
                    Administrator admin = admindao.adminFromID(row._Id);
                    if (admin == null)
                    {
                        Console.WriteLine("Null je");
                        userWindow uw = new userWindow();
                        uw.Show();

                    }
                    else
                    {
                        userWindow uw = new userWindow(admin);
                        uw.Show();
                    }
                    this.Close();
                }
                else if (row.Tip == TypeUser.TA)
                {
                    AsistentDAO asistentdao = new AsistentDAO();
                    Console.WriteLine((int)row._Id);
                    Asistent asistent1 = asistentdao.asistentFromID((int)row._Id);
                    if (asistent1 == null)
                    {
                        userWindow uw = new userWindow();
                        uw.Show();
                    }
                    else
                    {
                        userWindow uw = new userWindow(asistent1);
                        uw.Show();
                    }
                    this.Close();
                }
                else if(row.Tip == TypeUser.Profesor)
                {
                    ProfesorDAO profadao = new ProfesorDAO();
                    Profesor profa = profadao.profaFromID((int)row._Id);
                    if (profa == null)
                    {
                        userWindow uw = new userWindow();
                        uw.Show();
                    }
                    else
                    {
                        userWindow uw = new userWindow(profa);
                        uw.Show();
                    }
                    this.Close();
                }
            }
            else
            {
                prikaziError("Odaberite korisnika kojeg zelite da izmenite");
            }
        }
    }
}
