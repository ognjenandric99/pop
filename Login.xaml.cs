﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using SF53_2018_POP2019.Model;
using SF53_2018_POP2019.Model.Dao;


namespace SF53_2018_POP2019
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login : Window
    {
        public Login()
        {
            InitializeComponent();
        }

        private void login(object sender, RoutedEventArgs e)
        {
            String username = login_username.Text;
            String password = login_password.Text;
            UsersDAO userdao = new UsersDAO();
            User user = userdao.login(username, password);
            if (user != null)
            {
                Application.Current.Resources.Add("ID", user._Id);
                Application.Current.Resources.Add("Username", user.Username);
                if (user.Tip == TypeUser.Admin)
                {
                    MainWindow mw = new MainWindow();
                    mw.Show();
                }
                else if(user.Tip == TypeUser.Profesor)
                {
                    proceedProfesor();
                }
                else if(user.Tip == TypeUser.TA)
                {
                    proceedTA();
                }
                this.Close();
            }
            else
            {
                errormsg.Content = "Molimo Vas da unesete tacne podatke.";
            }
        }

        private void proceedUnregistered(object sender, RoutedEventArgs e)
        {
            window_neregistrovan.glavniProzor neregGlavniProzor = new window_neregistrovan.glavniProzor();
            neregGlavniProzor.Show();
            this.Close();
        }

        private void proceedProfesor()
        {
            window_profesor.glavniProzor profProzor = new window_profesor.glavniProzor();
            profProzor.Show();
            this.Close();
        }

        private void proceedTA()
        {
            window_ta.glavniProzor taProzor = new window_ta.glavniProzor();
            taProzor.Show();
            this.Close();
        }
    }
}
