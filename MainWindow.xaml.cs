﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using SF53_2018_POP2019.Model;
using SF53_2018_POP2019.Util;
using SF53_2018_POP2019.Model.Dao;

namespace SF53_2018_POP2019
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private List<User> _listaKorisnika;

        public List<User> ListaKorisnika
        {
            get { return _listaKorisnika; }
            set { _listaKorisnika = value; }
        }

        public MainWindow()
        {
            InitializeComponent();
        }

        private void korisnikeOtvori(object sender, RoutedEventArgs e)
        {
            KorisniciWindow window = new KorisniciWindow();
            window.Show();
        }

        private void ustanoveOtvori(object sender, RoutedEventArgs e)
        {
            UstanoveWindow uw = new UstanoveWindow();
            uw.Show();
        }
    }
}
