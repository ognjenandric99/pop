﻿using SF53_2018_POP2019.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF53_2018_POP2019.Model.Dao
{
    public class AdminDAO
    {
        public int CreateUser(User user)
        {

            return 1;
        }

        public List<Administrator> listaAdmina = new List<Administrator>();

        public Boolean deleteAdmin(int id)
        {
            using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"UPDATE Users SET Status='Deleted' WHERE Id=@id";
                command.Parameters.Add(new SqlParameter("id", id));
     
                int red = command.ExecuteNonQuery();
                if (red == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public Boolean newAdmin(Administrator admin)
        {
            Administrator user = admin;
            if(user.Username!="" && user.Password != "" && user.Email != "" && user.Ime != "" && user.Prezime != "" && user.Tip!=null && user.Status!=null)
            {
                using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
                {
                    conn.Open();
                    SqlCommand command = conn.CreateCommand();
                    command.CommandText = @"INSERT INTO Users(Id,Username,Password,Email,Ime,Prezime,Status,Tip,Povezanost) VALUES (@id,@username,@password,@email,@ime,@prezime,@status,@tip,@povezanost)";
                    command.Parameters.Add(new SqlParameter("id", admin._Id));
                    command.Parameters.Add(new SqlParameter("username", admin.Username));
                    command.Parameters.Add(new SqlParameter("password", admin.Password));
                    command.Parameters.Add(new SqlParameter("email", admin.Email));
                    command.Parameters.Add(new SqlParameter("ime", admin.Ime));
                    command.Parameters.Add(new SqlParameter("prezime", admin.Prezime));

                    String status = "Deleted";
                    if (admin.Status)
                    {
                        status = "Active";
                    }
                    command.Parameters.Add(new SqlParameter("status", status));
                    command.Parameters.Add(new SqlParameter("tip", admin.Tip.ToString()));
                    command.Parameters.Add(new SqlParameter("povezanost", "Null"));

                    int red = command.ExecuteNonQuery();
                    if (red == 1)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            else
            {
                return false;
            }
        }
        public Boolean saveEdit(Administrator admin)
        {
            Administrator user = admin;
            if (user.Username != "" && user.Password != "" && user.Email != "" && user.Ime != "" && user.Prezime != "" && user.Tip != null && user.Status != null)
            {
                using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
                {
                    conn.Open();
                    SqlCommand command = conn.CreateCommand();
                    command.CommandText = @"UPDATE Users SET Username=@username,Password=@password,Email=@email,Ime=@ime,Prezime=@prezime,Status=@status,Tip=@tip WHERE Id=@id";
                    command.Parameters.Add(new SqlParameter("username", admin.Username));
                    command.Parameters.Add(new SqlParameter("password", admin.Password));
                    command.Parameters.Add(new SqlParameter("email", admin.Email));
                    command.Parameters.Add(new SqlParameter("ime", admin.Ime));
                    command.Parameters.Add(new SqlParameter("prezime", admin.Prezime));
                    String status = "Deleted";
                    if (admin.Status)
                    {
                        status = "Active";
                    }
                    command.Parameters.Add(new SqlParameter("status", status));
                    command.Parameters.Add(new SqlParameter("tip", admin.Tip.ToString()));
                    command.Parameters.Add(new SqlParameter("id", admin._Id));
                    int red = command.ExecuteNonQuery();
                    if (red == 1)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            else
            {
                return false;
            }
                
        }

        public Administrator adminFromID(int id)
        {
            using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
            {
                conn.Open();
                DataSet ds = new DataSet();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = "SELECT * FROM Users WHERE Tip='Admin' AND ID=" + id;

                SqlDataAdapter dataAdapter = new SqlDataAdapter();
                dataAdapter.SelectCommand = command;
                dataAdapter.Fill(ds, "Users");

                Console.WriteLine("XD : " + ds.Tables["Users"].Rows.Count.ToString());

                try
                {
                    Administrator admin = new Administrator();
                    foreach (DataRow row in ds.Tables["Users"].Rows)
                    {
                        
                        admin._Id = (int)row["Id"];
                        admin.Username = (string)row["Username"];
                        admin.Password = (string)row["Password"];
                        admin.Email = (string)row["Email"];
                        admin.Ime = (string)row["Ime"];
                        admin.Prezime = (string)row["Prezime"];
                        String status = (string)row["Status"];
                        if (status == "Active")
                        {
                            admin.Status = true;
                        }
                        else
                        {
                            admin.Status = false;
                        }
                        //TypeUser tip = (TypeUser)Enum.Parse(typeof(String), (string) row["Tip"], true);
                        String tip1 = (string)row["Tip"];

                        TypeUser tip = TypeUser.TA;
                        if (tip1.Equals("Admin"))
                        {
                            tip = TypeUser.Admin;
                        }
                        else if (tip1.Equals("Profesor"))
                        {
                            tip = TypeUser.Profesor;
                        }
                        else if (tip1.Equals("Asistent"))
                        {
                            tip = TypeUser.TA;
                        }

                        admin.Tip = tip;
                        
                    }
                    return admin;
                }
                catch (Exception)
                {
                    return null;
                }
            }
                
        }

        public List<Administrator> selectAdmins()
        {
            using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
            {
                conn.Open();
                DataSet ds = new DataSet();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = "SELECT * FROM Users WHERE Tip=Admin";

                SqlDataAdapter dataAdapter = new SqlDataAdapter();
                dataAdapter.SelectCommand = command;
                dataAdapter.Fill(ds, "Users");

                List<Administrator> listaAdmina = new List<Administrator>();

                foreach (DataRow row in ds.Tables["Users"].Rows)
                {
                    Administrator user = new Administrator();
                    user._Id = (int)row["Id"];
                    user.Username = (string)row["Username"];
                    user.Password = (string)row["Password"];
                    user.Email = (string)row["Email"];
                    user.Ime = (string)row["Ime"];
                    user.Prezime = (string)row["Prezime"];
                    String status = (string)row["Status"];
                    if (status == "Active")
                    {
                        user.Status = true;
                    }
                    else
                    {
                        user.Status = false;
                    }
                    //TypeUser tip = (TypeUser)Enum.Parse(typeof(String), (string) row["Tip"], true);
                    String tip1 = (string)row["Tip"];

                    TypeUser tip = TypeUser.TA;
                    if (tip1.Equals("Admin"))
                    {
                        tip = TypeUser.Admin;
                    }
                    else if (tip1.Equals("Profesor"))
                    {
                        tip = TypeUser.Profesor;
                    }
                    else if (tip1.Equals("Asistent"))
                    {
                        tip = TypeUser.TA;
                    }

                    user.Tip = tip;
                    listaAdmina.Add(user);
                }
                Console.WriteLine(listaAdmina.Count);


                return listaAdmina;
            }

        }
    }
}
