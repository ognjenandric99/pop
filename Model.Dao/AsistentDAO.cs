﻿using SF53_2018_POP2019.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF53_2018_POP2019.Model.Dao
{
    public class AsistentDAO
    {

        public Boolean newAsistent(Asistent admin)
        {
            Asistent user = admin;
            if (user.Username != "" && user.Password != "" && user.Email != "" && user.Ime != "" && user.Prezime != "" && user.Tip != null && user.Status != null && user.Profesor!="")
            {
                using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
                {
                    conn.Open();
                    SqlCommand command = conn.CreateCommand();
                    command.CommandText = @"INSERT INTO Users(Id,Username,Password,Email,Ime,Prezime,Status,Tip,Povezanost) VALUES (@id,@username,@password,@email,@ime,@prezime,@status,@tip,@profid)";
                    command.Parameters.Add(new SqlParameter("id", admin._Id));
                    command.Parameters.Add(new SqlParameter("username", admin.Username));
                    command.Parameters.Add(new SqlParameter("password", admin.Password));
                    command.Parameters.Add(new SqlParameter("email", admin.Email));
                    command.Parameters.Add(new SqlParameter("ime", admin.Ime));
                    command.Parameters.Add(new SqlParameter("prezime", admin.Prezime));

                    String status = "Deleted";
                    if (admin.Status)
                    {
                        status = "Active";
                    }
                    command.Parameters.Add(new SqlParameter("status", status));
                    command.Parameters.Add(new SqlParameter("tip", "TA"));
                    command.Parameters.Add(new SqlParameter("profid", admin.Profesor));

                    int red = command.ExecuteNonQuery();
                    if (red == 1)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            else
            {
                return false;
            }
        }

        public Boolean saveEdit(Asistent asistent)
        {
            Asistent user = asistent;
            if (user.Username != "" && user.Password != "" && user.Email != "" && user.Ime != "" && user.Prezime != "" && user.Tip != null && user.Status != null)
            {
                using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
                {
                    conn.Open();
                    SqlCommand command = conn.CreateCommand();
                    command.CommandText = @"UPDATE Users SET Username=@username,Password=@password,Email=@email,Ime=@ime,Prezime=@prezime,Status=@status,Tip=@tip,Povezanost=@profid WHERE Id=@id";
                    command.Parameters.Add(new SqlParameter("username", asistent.Username));
                    command.Parameters.Add(new SqlParameter("password", asistent.Password));
                    command.Parameters.Add(new SqlParameter("email", asistent.Email));
                    command.Parameters.Add(new SqlParameter("ime", asistent.Ime));
                    command.Parameters.Add(new SqlParameter("prezime", asistent.Prezime));
                    String status = "Deleted";
                    if (asistent.Status)
                    {
                        status = "Active";
                    }
                    command.Parameters.Add(new SqlParameter("status", status));
                    command.Parameters.Add(new SqlParameter("tip", asistent.Tip.ToString()));
                    command.Parameters.Add(new SqlParameter("id", asistent._Id));
                    command.Parameters.Add(new SqlParameter("profid", asistent.Profesor));
                    int red = command.ExecuteNonQuery();
                    if (red == 1)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            else
            {
                return false;
            }

        }

        public Asistent asistentFromID(int id)
        {
            using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
            {
                conn.Open();
                DataSet ds = new DataSet();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = "SELECT * FROM Users WHERE Tip='TA' AND ID=" + id;

                SqlDataAdapter dataAdapter = new SqlDataAdapter();
                dataAdapter.SelectCommand = command;
                dataAdapter.Fill(ds, "Users");

                Console.WriteLine("XD : " + ds.Tables["Users"].Rows.Count.ToString());

                try
                {
                    Asistent Asistent = new Asistent();
                    foreach (DataRow row in ds.Tables["Users"].Rows)
                    {

                        Asistent._Id = (int)row["Id"];
                        Asistent.Username = (string)row["Username"];
                        Asistent.Password = (string)row["Password"];
                        Asistent.Email = (string)row["Email"];
                        Asistent.Ime = (string)row["Ime"];
                        Asistent.Prezime = (string)row["Prezime"];
                        String status = (string)row["Status"];
                        if (status == "Active")
                        {
                            Asistent.Status = true;
                        }
                        else
                        {
                            Asistent.Status = false;
                        }
                        try
                        {
                            Asistent.Profesor = (string)row["Povezanost"];
                        }
                        catch (Exception)
                        {
                            Asistent.Profesor = "0";
                        }
                        //TypeUser tip = (TypeUser)Enum.Parse(typeof(String), (string) row["Tip"], true);
                        String tip1 = (string)row["Tip"];

                        TypeUser tip = TypeUser.TA;
                        if (tip1.Equals("Admin"))
                        {
                            tip = TypeUser.Admin;
                        }
                        else if (tip1.Equals("Profesor"))
                        {
                            tip = TypeUser.Profesor;
                        }
                        else if (tip1.Equals("Asistent"))
                        {
                            tip = TypeUser.TA;
                        }

                        Asistent.Tip = tip;

                    }
                    return Asistent;
                }
                catch (Exception)
                {
                    return null;
                }
            }

        }

    }
}
