﻿using SF53_2018_POP2019.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF53_2018_POP2019.Model.Dao
{
    public class ProfesorDAO
    {
        

        public Boolean newProfesor(Profesor profa)
        {
            Profesor user = profa;
            if (user.Username != "" && user.Password != "" && user.Email != "" && user.Ime != "" && user.Prezime != "" && user.Tip == TypeUser.Profesor && user.Status != null && user.Asistenti == null)
            {
                using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
                {
                    conn.Open();
                    SqlCommand command = conn.CreateCommand();
                    command.CommandText = @"INSERT INTO Users(Id,Username,Password,Email,Ime,Prezime,Status,Tip,Povezanost) VALUES (@id,@username,@password,@email,@ime,@prezime,@status,@tip,@povezanost)";
                    command.Parameters.Add(new SqlParameter("id", profa._Id));
                    command.Parameters.Add(new SqlParameter("username", profa.Username));
                    command.Parameters.Add(new SqlParameter("password", profa.Password));
                    command.Parameters.Add(new SqlParameter("email", profa.Email));
                    command.Parameters.Add(new SqlParameter("ime", profa.Ime));
                    command.Parameters.Add(new SqlParameter("prezime", profa.Prezime));

                    String status = "Deleted";
                    if (profa.Status)
                    {
                        status = "Active";
                    }
                    command.Parameters.Add(new SqlParameter("status", status));
                    command.Parameters.Add(new SqlParameter("tip", "Profesor"));
                    command.Parameters.Add(new SqlParameter("povezanost", "0"));

                    int red = command.ExecuteNonQuery();
                    if (red == 1)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            else
            {
                return false;
            }
        }

        public Boolean saveEdit(Profesor profa)
        {
            Profesor user = profa;
            if (user.Username != "" && user.Password != "" && user.Email != "" && user.Ime != "" && user.Prezime != "" && user.Tip == TypeUser.Profesor)
            {
                using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
                {
                    conn.Open();
                    SqlCommand command = conn.CreateCommand();
                    command.CommandText = @"UPDATE Users SET Username=@username,Password=@password,Email=@email,Ime=@ime,Prezime=@prezime,Status=@status,Tip=@tip,Povezanost=@povezanost WHERE Id=@id";
                    command.Parameters.Add(new SqlParameter("username", profa.Username));
                    command.Parameters.Add(new SqlParameter("password", profa.Password));
                    command.Parameters.Add(new SqlParameter("email", profa.Email));
                    command.Parameters.Add(new SqlParameter("ime", profa.Ime));
                    command.Parameters.Add(new SqlParameter("prezime", profa.Prezime));
                    String status = "Deleted";
                    if (profa.Status)
                    {
                        status = "Active";
                    }
                    command.Parameters.Add(new SqlParameter("status", status));
                    command.Parameters.Add(new SqlParameter("tip", profa.Tip.ToString()));
                    command.Parameters.Add(new SqlParameter("id", profa._Id));
                    if(profa.Asistenti.Count==0){
                        profa.Asistenti.Add("0");
                    }
                    command.Parameters.Add(new SqlParameter("povezanost", string.Join(",", profa.Asistenti.ToArray())));
                    int red = command.ExecuteNonQuery();
                    if (red == 1)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            else
            {
                return false;
            }

        }

        public Profesor profaFromID(int id)
        {
            using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
            {
                conn.Open();
                DataSet ds = new DataSet();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = "SELECT * FROM Users WHERE Tip='Profesor' AND ID=" + id;

                SqlDataAdapter dataAdapter = new SqlDataAdapter();
                dataAdapter.SelectCommand = command;
                dataAdapter.Fill(ds, "Users");

                Console.WriteLine("XD : " + ds.Tables["Users"].Rows.Count.ToString());

                try
                {
                    Profesor profa = new Profesor();
                    foreach (DataRow row in ds.Tables["Users"].Rows)
                    {

                        profa._Id = (int)row["Id"];
                        profa.Username = (string)row["Username"];
                        profa.Password = (string)row["Password"];
                        profa.Email = (string)row["Email"];
                        profa.Ime = (string)row["Ime"];
                        profa.Prezime = (string)row["Prezime"];
                        String status = (string)row["Status"];
                        if (status == "Active")
                        {
                            profa.Status = true;
                        }
                        else
                        {
                            profa.Status = false;
                        }
                        try
                        {
                            profa.Asistenti = new List<String> { (string)row["Povezanost"] };
                        }
                        catch (Exception)
                        {
                            profa.Asistenti = null;
                            
                        }
                        //TypeUser tip = (TypeUser)Enum.Parse(typeof(String), (string) row["Tip"], true);
                        String tip1 = (string)row["Tip"];

                        TypeUser tip = TypeUser.TA;
                        if (tip1.Equals("Admin"))
                        {
                            tip = TypeUser.Admin;
                        }
                        else if (tip1.Equals("Profesor"))
                        {
                            tip = TypeUser.Profesor;
                        }
                        else if (tip1.Equals("Asistent"))
                        {
                            tip = TypeUser.TA;
                        }

                        profa.Tip = tip;

                    }
                    return profa;
                }
                catch (Exception)
                {
                    return null;
                }
            }

        }

    }
}
