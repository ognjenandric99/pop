﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using SF53_2018_POP2019.Util;
using System.Text;
using System.Threading.Tasks;

namespace SF53_2018_POP2019.Model.Dao
{
    public class TerminDAO
    {
        public TerminDAO()
        {

        }

        public List<Termin> selectTermins(Ustanova ust)
        {
            List<Termin> lista = new List<Termin>();

            try
            {
                using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
                {
                    conn.Open();
                    DataSet ds = new DataSet();
                    SqlCommand command = conn.CreateCommand();
                    command.CommandText = "SELECT * FROM Termini WHERE Ustanova=@ustanova";
                    command.Parameters.Add(new SqlParameter("ustanova", ust._Id.ToString()));


                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.SelectCommand = command;
                    dataAdapter.Fill(ds, "Termini");
                    Console.WriteLine("Vratilo : " + ds.Tables["Termini"].Rows.Count);
                    foreach (DataRow row in ds.Tables["Termini"].Rows)
                    {
                        Termin t = new Termin();
                        t.Sifra = int.Parse(row["Id"].ToString());
                        t.Pocetak = (DateTime)row["Pocetak"];
                        t.Kraj = (DateTime)row["Kraj"];
                        t.TipNastave = row["Tip"].ToString();
                        t.Korisnik = row["Korisnik"].ToString();
                        t.Ustanova = row["Ustanova"].ToString();
                        t.Ucionica = row["Ucionica"].ToString();
                        lista.Add(t);
                    }
                }
            }
            catch
            {
                lista = new List<Termin>();
            }


            return lista;
        }

        public Boolean editTermin(Termin termin)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
                {
                    conn.Open();
                    SqlCommand command = conn.CreateCommand();
                    command.CommandText = @"UPDATE Termini SET Pocetak=@pocetak,Kraj=@kraj,Tip=@tip,Korisnik=@korisnik,Ustanova=@ustanova,Ucionica=@ucionica WHERE Id=@id";
                    command.Parameters.Add(new SqlParameter("id", termin.Sifra));
                    command.Parameters.Add(new SqlParameter("pocetak", termin.Pocetak));
                    command.Parameters.Add(new SqlParameter("kraj", termin.Kraj));
                    command.Parameters.Add(new SqlParameter("tip", termin.TipNastave));
                    command.Parameters.Add(new SqlParameter("korisnik", termin.Korisnik));
                    command.Parameters.Add(new SqlParameter("ustanova", termin.Ustanova));
                    command.Parameters.Add(new SqlParameter("ucionica", termin.Ucionica));
                    int red = command.ExecuteNonQuery();
                    if (red == 1)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch
            {
                return false;
            }
        }
        public Boolean newTermin(Termin termin)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
                {
                    conn.Open();
                    SqlCommand command = conn.CreateCommand();
                    command.CommandText = @"INSERT INTO Termini(Id,Pocetak,Kraj,Tip,Korisnik,Ustanova,Ucionica) VALUES (@id,@pocetak,@kraj,@tip,@korisnik,@ustanova,@ucionica)";
                    command.Parameters.Add(new SqlParameter("id", termin.Sifra));
                    command.Parameters.Add(new SqlParameter("pocetak", termin.Pocetak));
                    command.Parameters.Add(new SqlParameter("kraj", termin.Kraj));
                    command.Parameters.Add(new SqlParameter("tip", termin.TipNastave));
                    command.Parameters.Add(new SqlParameter("korisnik", termin.Korisnik));
                    command.Parameters.Add(new SqlParameter("ustanova", termin.Ustanova));
                    command.Parameters.Add(new SqlParameter("ucionica", termin.Ucionica));
                    int red = command.ExecuteNonQuery();
                    if (red == 1)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch
            {
                return false;
            }
        }

        public int nextId()
        {
            using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
            {
                conn.Open();
                DataSet ds = new DataSet();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = "SELECT * FROM Termini";

                SqlDataAdapter dataAdapter = new SqlDataAdapter();
                dataAdapter.SelectCommand = command;
                dataAdapter.Fill(ds, "Termini");

                List<User> listaKorisnika = new List<User>();

                return ((int)ds.Tables["Termini"].Rows.Count + 1);
            }
        }
    }
}
