﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SF53_2018_POP2019.Model;
using SF53_2018_POP2019.Util;
using System.Data.SqlClient;
using System.Data;

namespace SF53_2018_POP2019.Model.Dao
{
    public class UcionicaDAO
    {
        public UcionicaDAO()
        {

        }

        public Boolean sacuvajUcionicu(Ucionica uc)
        {
            Console.WriteLine(uc.Oznaka);
            return false;
        }
        public Ucionica ucionicaFromId(String id)
        {
            Ucionica uc = new Ucionica();
            try
            {
                using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
                {
                    conn.Open();
                    DataSet ds = new DataSet();
                    SqlCommand command = conn.CreateCommand();
                    command.CommandText = "SELECT * FROM Ucionice WHERE Id=@id";
                    command.Parameters.Add(new SqlParameter("id", id));
                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.SelectCommand = command;
                    dataAdapter.Fill(ds, "Ucionice");

                    foreach(DataRow row in ds.Tables["Ucionice"].Rows)
                    {
                        uc.Id = (int)row["Id"];
                        uc.Oznaka = row["Oznaka"].ToString();
                        uc.Ustanova = row["Ustanova"].ToString();
                        uc.BrojMesta = int.Parse(row["Broj_mesta"].ToString());
                        uc.TipUcionice = row["Tip"].ToString();
                    }
                }
            }
            catch
            {
                uc = null;
            }
            return uc;
        }
        public int nextId()
        {
            using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
            {
                conn.Open();
                DataSet ds = new DataSet();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = "SELECT * FROM Ucionice";

                SqlDataAdapter dataAdapter = new SqlDataAdapter();
                dataAdapter.SelectCommand = command;
                dataAdapter.Fill(ds, "Ucionice");

                List<User> listaKorisnika = new List<User>();

                return ((int)ds.Tables["Ucionice"].Rows.Count + 1);
            }
        }
        public List<Ucionica> selectUcionice(Ustanova ust)
        {
            List<Ucionica> lista = new List<Ucionica>();

            try
            {
                using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
                {
                    conn.Open();
                    DataSet ds = new DataSet();
                    SqlCommand command = conn.CreateCommand();
                    command.CommandText = "SELECT * FROM Ucionice WHERE Ustanova=@ustanova";
                    command.Parameters.Add(new SqlParameter("ustanova", ust._Id.ToString()));


                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.SelectCommand = command;
                    dataAdapter.Fill(ds, "Ucionice");
                    Console.WriteLine("Vratilo : " + ds.Tables["Ucionice"].Rows.Count);
                    foreach (DataRow row in ds.Tables["Ucionice"].Rows)
                    {
                        Ucionica uc = new Ucionica();
                        uc.Id = (int) row["Id"];
                        uc.Oznaka = row["Oznaka"].ToString();
                        uc.Ustanova = row["Ustanova"].ToString();
                        uc.BrojMesta = int.Parse(row["Broj_mesta"].ToString());
                        uc.TipUcionice = row["Tip"].ToString();
                        lista.Add(uc);
                    }
                }
            }
            catch
            {
                lista = new List<Ucionica>();
            }

            return lista;
        }

        public Boolean newUcionica(Ustanova ust, Ucionica uc)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
                {
                    conn.Open();
                    SqlCommand command = conn.CreateCommand();
                    command.CommandText = @"INSERT INTO Ucionice(Id,Oznaka,Ustanova,Broj_Mesta,Tip) VALUES (@id,@oznaka,@ustanova,@broj,@tip)";
                    command.Parameters.Add(new SqlParameter("id", uc.Id));
                    command.Parameters.Add(new SqlParameter("oznaka",uc.Oznaka));
                    command.Parameters.Add(new SqlParameter("ustanova", ust._Id));
                    command.Parameters.Add(new SqlParameter("broj",uc.BrojMesta));
                    command.Parameters.Add(new SqlParameter("tip",uc.TipUcionice));
                    int red = command.ExecuteNonQuery();
                    if (red == 1)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch
            {
                return false;
            }
        }

        public Boolean editUcionica(Ustanova ust,Ucionica uc)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
                {
                    conn.Open();
                    SqlCommand command = conn.CreateCommand();
                    command.CommandText = @"UPDATE Ucionice SET Oznaka=@oznaka,Ustanova=@ustanova,Broj_mesta=@broj,Tip=@tip WHERE Id=@id";
                    command.Parameters.Add(new SqlParameter("id", uc.Id));
                    command.Parameters.Add(new SqlParameter("oznaka", uc.Oznaka));
                    command.Parameters.Add(new SqlParameter("ustanova", ust._Id));
                    command.Parameters.Add(new SqlParameter("broj", uc.BrojMesta));
                    command.Parameters.Add(new SqlParameter("tip", uc.TipUcionice));
                    int red = command.ExecuteNonQuery();
                    if (red == 1)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch
            {
                return false;
            }
        }


    }
}
