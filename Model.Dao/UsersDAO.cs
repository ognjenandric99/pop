﻿using SF53_2018_POP2019.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF53_2018_POP2019.Model.Dao
{
    public class UsersDAO
    {
        public int CreateUser(User user)
        {

            return 1;
        }

        public List<User> listaUsera = new List<User>();

        public Boolean deleteUser(int id)
        {
            using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"UPDATE Users SET Status='Deleted' WHERE Id=@id";
                command.Parameters.Add(new SqlParameter("id", id));

                int red = command.ExecuteNonQuery();
                if (red == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public User login(String username,String password)
        {
            using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
            {
                conn.Open();
                DataSet ds = new DataSet();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = "SELECT * FROM Users WHERE Username=@username AND Password=@password";
                command.Parameters.Add(new SqlParameter("username", username));
                command.Parameters.Add(new SqlParameter("password", password));

                SqlDataAdapter dataAdapter = new SqlDataAdapter();
                dataAdapter.SelectCommand = command;
                dataAdapter.Fill(ds, "Users");

                if ((int)ds.Tables["Users"].Rows.Count == 1)
                {
                    foreach (DataRow row in ds.Tables["Users"].Rows)
                    {
                        User user = new User();
                        user._Id = (int)row["Id"];
                        user.Username = (string)row["Username"];
                        user.Password = (string)row["Password"];
                        user.Email = (string)row["Email"];
                        user.Ime = (string)row["Ime"];
                        user.Prezime = (string)row["Prezime"];
                        String status = (string)row["Status"];
                        if (status == "Active")
                        {
                            user.Status = true;
                        }
                        else
                        {
                            user.Status = false;
                        }
                        //TypeUser tip = (TypeUser)Enum.Parse(typeof(String), (string) row["Tip"], true);
                        String tip1 = (string)row["Tip"];

                        TypeUser tip = TypeUser.TA;
                        if (tip1.Equals("Admin"))
                        {
                            tip = TypeUser.Admin;
                        }
                        else if (tip1.Equals("Profesor"))
                        {
                            tip = TypeUser.Profesor;
                        }
                        else if (tip1.Equals("Asistent"))
                        {
                            tip = TypeUser.TA;
                        }

                        user.Tip = tip;
                        return user;
                    }
                }
                else
                {
                    return null;
                }
            }
                return null;
        }

        public int newId()
        {
            using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
            {
                conn.Open();
                DataSet ds = new DataSet();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = "SELECT * FROM Users";

                SqlDataAdapter dataAdapter = new SqlDataAdapter();
                dataAdapter.SelectCommand = command;
                dataAdapter.Fill(ds, "Users");

                List<User> listaKorisnika = new List<User>();

                return ((int)ds.Tables["Users"].Rows.Count + 1);
            }
        }
        public List<User> selectUsers()
        {
            using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
            {
                conn.Open();
                DataSet ds = new DataSet();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = "SELECT * FROM Users";

                SqlDataAdapter dataAdapter = new SqlDataAdapter();
                dataAdapter.SelectCommand = command;
                dataAdapter.Fill(ds, "Users");

                List<User> listaKorisnika = new List<User>();

                foreach(DataRow row in ds.Tables["Users"].Rows)
                {
                    User user = new User();
                    user._Id = (int) row["Id"];
                    user.Username =(string) row["Username"];
                    user.Password = (string)row["Password"];
                    user.Email = (string)row["Email"];
                    user.Ime = (string)row["Ime"];
                    user.Prezime = (string)row["Prezime"];
                    String status = (string)row["Status"];
                    if (status == "Active")
                    {
                        user.Status = true;
                    }
                    else
                    {
                        user.Status = false;
                    }
                    //TypeUser tip = (TypeUser)Enum.Parse(typeof(String), (string) row["Tip"], true);
                    String tip1 = (string)row["Tip"];

                    TypeUser tip = TypeUser.TA;
                    if (tip1.Equals("Admin"))
                    {
                         tip = TypeUser.Admin;
                    }
                    else if (tip1.Equals("Profesor"))
                    {
                         tip = TypeUser.Profesor;
                    }
                    else if (tip1.Equals("TA"))
                    {
                         tip = TypeUser.TA;
                    }

                    user.Tip = tip;
                    listaKorisnika.Add(user);
                }
                Console.WriteLine(listaKorisnika.Count);


                return listaKorisnika;
            }

        }
    }
}
