﻿using SF53_2018_POP2019.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF53_2018_POP2019.Model.Dao
{
    class UstanovaDAO
    {
        public List<Ustanova> selectUstanove()
        {
            using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
            {
                conn.Open();
                DataSet ds = new DataSet();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = "SELECT * FROM Ustanove";

                SqlDataAdapter dataAdapter = new SqlDataAdapter();
                dataAdapter.SelectCommand = command;
                dataAdapter.Fill(ds, "Ustanove");

                List<Ustanova> listaUstanova = new List<Ustanova>();

                foreach (DataRow row in ds.Tables["Ustanove"].Rows)
                {
                    Ustanova ustanova = new Ustanova();
                    ustanova._Id = (int)row["Id"];
                    ustanova._Naziv = (string)row["Naziv"];
                    ustanova._Adresa = (string)row["Adresa"];
                    ustanova._Status = (string)row["Status"];

                    listaUstanova.Add(ustanova);
                }
                Console.WriteLine(listaUstanova.Count);


                return listaUstanova;
            }

        }

        public Boolean deleteUstanova(int id)
        {
            using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"UPDATE Ustanove SET Status='Deleted' WHERE Id=@id";
                command.Parameters.Add(new SqlParameter("id", id));

                int red = command.ExecuteNonQuery();
                if (red == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public int newId()
        {
            using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
            {
                conn.Open();
                DataSet ds = new DataSet();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = "SELECT * FROM Ustanove";

                SqlDataAdapter dataAdapter = new SqlDataAdapter();
                dataAdapter.SelectCommand = command;
                dataAdapter.Fill(ds, "Ustanove");

                return ((int)ds.Tables["Ustanove"].Rows.Count + 1);
            }
        }

        
        public Boolean newUstanova(Ustanova ust)
        {
            try
            {
                if (ust == null || ust._Id != newId() || ust._Naziv.Length<=0 || ust._Adresa.Length<=0)
                {
                    return false;
                }
                using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
                {
                    conn.Open();
                    SqlCommand command = conn.CreateCommand();
                    command.CommandText = @"INSERT INTO Ustanove(Id,Naziv,Adresa,Status) VALUES(@id,@naziv,@adresa,@status)";
                    command.Parameters.Add(new SqlParameter("id", ust._Id.ToString()));
                    command.Parameters.Add(new SqlParameter("naziv", ust._Naziv));
                    command.Parameters.Add(new SqlParameter("adresa", ust._Adresa));
                    command.Parameters.Add(new SqlParameter("status",ust._Status));
                    int red = command.ExecuteNonQuery();
                    if (red == 1)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch
            {
                return false;
            }
            
        }
        public Boolean editUstanova(Ustanova ust)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
                {
                    conn.Open();
                    SqlCommand command = conn.CreateCommand();
                    command.CommandText = @"UPDATE Ustanove SET Naziv=@naziv,Adresa=@adresa,Status=@status WHERE Id=@id";
                    command.Parameters.Add(new SqlParameter("id", ust._Id));
                    command.Parameters.Add(new SqlParameter("naziv", ust._Naziv));
                    command.Parameters.Add(new SqlParameter("adresa", ust._Adresa));
                    command.Parameters.Add(new SqlParameter("status", ust._Status));
                    int red = command.ExecuteNonQuery();
                    if (red == 1)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch
            {
                return false;
            }
        }
        public Ustanova ustanovaFromID(String ID)
        {
            Ustanova ust = null;
            using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
            {
                conn.Open();
                DataSet ds = new DataSet();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = "SELECT * FROM Ustanove WHERE ID=@id";
                command.Parameters.Add(new SqlParameter("id", ID));

                SqlDataAdapter dataAdapter = new SqlDataAdapter();
                dataAdapter.SelectCommand = command;
                dataAdapter.Fill(ds, "Ustanove");

                try
                {
                    foreach(DataRow row in ds.Tables["Ustanove"].Rows)
                    {
                        ust = new Ustanova();
                        ust._Id = (int)row["Id"];
                        ust._Naziv = (string)row["Naziv"];
                        ust._Adresa = (string)row["Adresa"];
                        ust._Status = (string)row["Status"];
                    }
                }
                catch
                {
                    ust = null;
                }
            }


            return ust;
        }
    }
}
