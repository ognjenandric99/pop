﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF53_2018_POP2019.Model
{
    public class Administrator : User
    {

        public Administrator(int Id,String username, String password, String ime, String prezime, String email, bool status, TypeUser tip) : base(Id,username, password, ime, prezime, email, status, tip)
        {
            
        }

        public Administrator()
        {

        }
    }
}
