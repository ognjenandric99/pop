﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF53_2018_POP2019.Model
{
    public class Asistent:User
    {
        private String _profesor;

        public String Profesor
        {
            get { return _profesor; }
            set { _profesor = value; }
        }

        public Asistent(int Id,String username, String password, String ime, String prezime, String email, bool status, TypeUser tip, String profesor) : base(Id,username, password, ime, prezime, email, status, tip)
        {
            this.Profesor = profesor;
        }
        public Asistent()
        {

        }

}
}
