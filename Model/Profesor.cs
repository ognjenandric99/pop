﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF53_2018_POP2019.Model
{
    public class Profesor : User
    {
        private List<String> _asistenti;

        public List<String> Asistenti
        {
            get { return _asistenti; }
            set { _asistenti = value; }
        }
        
        public Profesor(int Id,String username, String password, String ime, String prezime, String email, bool status, TypeUser tip, List<String> asistenti) : base(Id,username, password, ime, prezime, email, status, tip)
        {
            if (asistenti != null)
            {
                this.Asistenti = asistenti;
            }
            else
            {
                List<String> prazno = new List<String>();
                this.Asistenti = prazno;
            }
            
        }

        public Profesor()
        {
            List<String> prazno = new List<String>();
            this.Asistenti = prazno; 
        }

        

    }
}
