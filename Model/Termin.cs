﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF53_2018_POP2019.Model
{
    public class Termin
    {

        private int _Sifra;

        public int Sifra
        {
            get { return _Sifra; }
            set { _Sifra = value; }
        }

        private DateTime _Pocetak;

        public DateTime Pocetak
        {
            get { return _Pocetak; }
            set { _Pocetak = value; }
        }

        private DateTime _Kraj;

        public DateTime Kraj
        {
            get { return _Kraj; }
            set { _Kraj = value; }
        }

        private String _tipNastave;

        public String TipNastave
        {
            get { return _tipNastave; }
            set { _tipNastave = value; }
        }

        private String _korisnik;

        public String Korisnik
        {
            get { return _korisnik; }
            set { _korisnik = value; }
        }

        private String _ucionica;

        public String Ucionica
        {
            get { return _ucionica; }
            set { _ucionica = value; }
        }


        private String _ustanova;

        public String Ustanova
        {
            get { return _ustanova; }
            set { _ustanova = value; }
        }



        public Termin()
        {

        }

        public Termin(int id,DateTime pocetak,DateTime kraj,String tip,String korisnik,String ustanova,String ucionica)
        {
            this.Sifra = id;
            this.Pocetak = pocetak;
            this.Kraj = kraj;
            this.TipNastave = tip;
            this.Korisnik = korisnik;
            this.Ucionica = ucionica;
            this.Ustanova = ustanova;
        }
    }
}
