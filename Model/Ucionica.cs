﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF53_2018_POP2019.Model
{
    public class Ucionica
    {
        private int _Id;

        public int Id
        {
            get { return _Id; }
            set { _Id = value; }
        }

        private String _Oznaka;

        public String Oznaka
        {
            get { return _Oznaka; }
            set { _Oznaka = value; }
        }

        private int _BrojMesta;

        public int BrojMesta
        {
            get { return _BrojMesta; }
            set { _BrojMesta = value; }
        }

        private String _TipUcionice;

        public String TipUcionice
        {
            get { return _TipUcionice; }
            set { _TipUcionice = value; }
        }

        private String _Ustanova;
        public String Ustanova
        {
            get { return _Ustanova; }
            set { _Ustanova = value; }
        }

        public Ucionica()
        {

        }

        public Ucionica(int Id,String oznaka,int BrojMesta,String oznakaUstanove,String tip)
        {
            this.Id = Id;
            this.Oznaka = oznaka;
            this.BrojMesta = BrojMesta;
            this.Ustanova = oznakaUstanove;
            this.TipUcionice = tip;
        }



    }
}
