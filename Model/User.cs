﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF53_2018_POP2019.Model
{
    public class User
    {

        private int Id;

        public int _Id
        {
            get { return Id; }
            set { Id = value; }
        }


        private String _username;

        public String Username
        {
            get { return _username; }
            set { _username = value; }
        }

        private String _password;

        public String Password
        {
            get { return _password; }
            set { _password = value; }
        }

        private String _email;

        public String Email
        {
            get { return _email; }
            set { _email = value; }
        }
        private String _ime;

        public String Ime
        {
            get { return _ime; }
            set { _ime = value; }
        }

        private String _prezime;

        public String Prezime
        {
            get { return _prezime; }
            set { _prezime = value; }
        }

        private bool _status;

        public bool Status
        {
            get { return _status; }
            set { _status = value; }
        }

        private TypeUser _tip;

        public TypeUser Tip
        {
            get { return _tip; }
            set { _tip = value; }
        }

        public User(int Id,String username, String password, String ime, String prezime, String email, bool status, TypeUser tip)
        {
            this.Id = Id;
            this.Username = username;
            this.Password = password;
            this.Ime = ime;
            this.Prezime = prezime;
            this.Email = email;
            this.Status = status;
            this.Tip = tip;
        }
        public User()
        {

        }
  
    }
}
