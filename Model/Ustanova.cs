﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF53_2018_POP2019.Model
{
    public class Ustanova
    {


        private int Id;

        public int _Id
        {
            get { return Id; }
            set { Id = value; }
        }

        private String Naziv;

        public String _Naziv
        {
            get { return Naziv; }
            set { Naziv = value; }
        }

        private String Adresa;

        public String _Adresa
        {
            get { return Adresa; }
            set { Adresa = value; }
        }

        private String Status;

        public String _Status
        {
            get { return Status; }
            set { Status = value; }
        }


        public Ustanova(int id,String naziv,String adresa,String Status)
        {
            this.Id = id;
            this.Naziv = naziv;
            this.Adresa = adresa;
            this.Status = Status;
        }

        public Ustanova()
        {

        }

    }
}
