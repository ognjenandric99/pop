﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using SF53_2018_POP2019.Model;
using SF53_2018_POP2019.Model.Dao;

namespace SF53_2018_POP2019
{
    /// <summary>
    /// Interaction logic for Ucionice.xaml
    /// </summary>
    public partial class Ucionice : Window
    {
        public UcionicaDAO ucidao = new UcionicaDAO();
        private Ustanova ust;
        ICollectionView view;
        public Ucionice(Ustanova ust)
        {
            InitializeComponent();
            
            this.ust = ust;
            UcionicaDAO udao = new UcionicaDAO();
            
            view = CollectionViewSource.GetDefaultView(udao.selectUcionice(ust));
            Console.WriteLine("IMA  : " + udao.selectUcionice(ust).Count);
            tableUcionice.ItemsSource = view;
            tableUcionice.CanUserAddRows = false;
        }

        private void dodajUcBtn(object sender, RoutedEventArgs e)
        {
            editAddUcionica ea = new editAddUcionica(this.ust);
            ea.Show();
            this.Close();
        }

        private void izmeniucBtn(object sender, RoutedEventArgs e)
        {
            if (tableUcionice.SelectedItem != null)
            {
                Ucionica izabrana = (Ucionica)tableUcionice.SelectedItems[0];
                Ucionica spremna = ucidao.ucionicaFromId(izabrana.Id.ToString());

                editAddUcionica ea = new editAddUcionica(this.ust, spremna);
                ea.Show();
                this.Close();
            }
        }
    }
}
