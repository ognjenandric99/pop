﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.ComponentModel;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using SF53_2018_POP2019.Model;
using SF53_2018_POP2019.Model.Dao;

namespace SF53_2018_POP2019
{
    /// <summary>
    /// Interaction logic for UstanovaDetaljno.xaml
    /// </summary>
    public partial class UstanovaDetaljno : Window
    {
        private Ustanova ust;
        ICollectionView view;

        public UstanovaDetaljno(Ustanova ust)
        {
            this.ust = ust;
            InitializeComponent();
            ud_naziv.Content = ust._Naziv;
            ud_adresa.Content = ust._Adresa;

            TerminDAO tdao = new TerminDAO();
            view = CollectionViewSource.GetDefaultView(tdao.selectTermins(ust));
            tableTermini.ItemsSource = view;
            tableTermini.CanUserAddRows = false;


        }

        private void ucitajUcionice(object sender, RoutedEventArgs e)
        {
            Ucionice ucionice = new Ucionice(this.ust);
            ucionice.Show();
        }

        private void noviTermin(object sender, RoutedEventArgs e)
        {
            editAddTermin et = new editAddTermin(this.ust);
            et.Show();
            this.Close();

        }

        private void editTermin(object sender, RoutedEventArgs e)
        {
            if (tableTermini.SelectedItem != null)
            {
                Termin nterm = (Termin)tableTermini.SelectedItems[0];
                editAddTermin et = new editAddTermin(this.ust, nterm);
                et.Show();
                this.Close();
            }
        }
    }
}
