﻿using SF53_2018_POP2019.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using SF53_2018_POP2019.Util;
using SF53_2018_POP2019.Model;
using SF53_2018_POP2019.Model.Dao;
using System.Data;

namespace SF53_2018_POP2019
{
    /// <summary>
    /// Interaction logic for UstanoveWindow.xaml
    /// </summary>
    public partial class UstanoveWindow : Window
    {
        UstanovaDAO ustanovadao = new UstanovaDAO();
        public UstanoveWindow()
        {
            InitializeComponent();
            
            ucitajUTabelu(ustanovadao.selectUstanove());
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }

        private void obrisiUstanovu(object sender, RoutedEventArgs e)
        {
            if (tableUstanove.SelectedItem != null)
            {
                Ustanova row = (Ustanova)tableUstanove.SelectedItems[0];
                UstanovaDAO ustdao = new UstanovaDAO();
                Boolean obrisano = ustdao.deleteUstanova(row._Id);
                if (obrisano)
                {
                    UstanoveWindow uw = new UstanoveWindow();
                    uw.Show();
                    this.Close();
                }
                else
                {
                    prikaziError("Nije bilo moguce obrisati.");
                }
            }
            else
            {
                prikaziError("Odaberite korisnika kojeg zelite da obrisete");
            }
        }

        private void izmeniUstanovu(object sender, RoutedEventArgs e)
        {
            if (tableUstanove.SelectedItem != null)
            {
                Ustanova row = (Ustanova)tableUstanove.SelectedItems[0];
                Ustanova ustanova = ustanovadao.ustanovaFromID(row._Id.ToString());
                EditAddUstanova ea = new EditAddUstanova(ustanova);
                ea.Show();
                this.Close();

            }
        }

        private void dodajUstanovu(object sender, RoutedEventArgs e)
        {
            EditAddUstanova nw = new EditAddUstanova();
            nw.Show();
            this.Close();
        }

        private void ucitajUTabelu(List<Ustanova> listaUstanova)
        {
 
            tableUstanove.ItemsSource = CollectionViewSource.GetDefaultView(listaUstanova);
            tableUstanove.CanUserAddRows = false;
        }

        public void prikaziError(String error)
        {
            errorMsgLabel.Visibility = Visibility.Visible;
            errorMsgLabel.Content = error;
        }

        private void otvoriUstanovu(object sender, RoutedEventArgs e)
        {
            if (tableUstanove.SelectedItem != null)
            {
                Ustanova row = (Ustanova)tableUstanove.SelectedItems[0];
                Ustanova ustanova = ustanovadao.ustanovaFromID(row._Id.ToString());
                UstanovaDetaljno ud = new UstanovaDetaljno(ustanova);
                ud.Show();
                this.Close();

            }
        }
    }
}
