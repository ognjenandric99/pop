﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SF53_2018_POP2019.Model;

namespace SF53_2018_POP2019.Util
{
    public class Data
    {
        public static string ConnectionString = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=Projekat;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
        private static Data instance;
    public Data()
    {}

    public static Data getInstance()
        {
            if (instance == null)
            {
                instance = new Data();
            }
            return instance;
        }
    }
}
