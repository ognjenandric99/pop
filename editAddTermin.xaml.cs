﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using SF53_2018_POP2019.Model;
using SF53_2018_POP2019.Model.Dao;

namespace SF53_2018_POP2019
{
    /// <summary>
    /// Interaction logic for editAddTermin.xaml
    /// </summary>
    public partial class editAddTermin : Window
    {
        private Ustanova ust;
        public TerminDAO t_dao = new TerminDAO();
        public editAddTermin(Ustanova ust)
        {
            this.ust = ust;
            InitializeComponent();
            editAddTermin_SifraUstanove.Text = ust._Id.ToString();
        }
        public editAddTermin(Ustanova ust,Termin t)
        {
            this.ust = ust;
            InitializeComponent();
            editAddTermin_ID.Text = t.Sifra.ToString();
            editAddTermin_SifraUstanove.Text = ust._Id.ToString();
            editAddTermin_pocetak.SelectedDate = t.Pocetak;
            editAddTermin_pocetakVreme.Text = t.Pocetak.Hour+":"+t.Pocetak.Minute;
            editAddTermin_kraj.SelectedDate = t.Kraj;
            editAddTermin_krajVreme.Text = t.Kraj.Hour + ":" + t.Kraj.Minute;
            editAddTermin_OznakaUcionice.Text = t.Ucionica;
            editAddTermin_TipPredavanja.Text = t.TipNastave;
            

        }

        private void save(object sender, RoutedEventArgs e)
        {
            try
            {
                int tid = int.Parse(editAddTermin_ID.Text);
                int tnew = t_dao.nextId();

                Termin t = new Termin();
                //Pocetak
                int pocetak_god = editAddTermin_pocetak.SelectedDate.Value.Year;
                int pocetak_mes = editAddTermin_pocetak.SelectedDate.Value.Month;
                int pocetak_dan = editAddTermin_pocetak.SelectedDate.Value.Day;
                String[] pocetakVreme = editAddTermin_pocetakVreme.Text.Split(':');
                int pocetak_sat = int.Parse(pocetakVreme[0]);
                int pocetak_min = int.Parse(pocetakVreme[1]);
                DateTime pocetak = new DateTime(pocetak_god, pocetak_mes, pocetak_dan, pocetak_sat, pocetak_min,0);
                //Kraj
                int kraj_god = editAddTermin_kraj.SelectedDate.Value.Year;
                int kraj_mes = editAddTermin_kraj.SelectedDate.Value.Month;
                int kraj_dan = editAddTermin_kraj.SelectedDate.Value.Day;
                String[] krajVreme = editAddTermin_krajVreme.Text.Split(':');
                int kraj_sat = int.Parse(krajVreme[0]);
                int kraj_min = int.Parse(krajVreme[1]);
                DateTime kraj = new DateTime(kraj_god, kraj_mes, kraj_dan, kraj_sat, kraj_min, 0);

                t.Sifra = int.Parse(editAddTermin_ID.Text);
                t.Pocetak = pocetak;
                t.Kraj = kraj;

                t.TipNastave = editAddTermin_TipPredavanja.Text;
                t.Korisnik = Application.Current.Resources["Username"].ToString();
                t.Ustanova = editAddTermin_SifraUstanove.Text;
                t.Ucionica = editAddTermin_OznakaUcionice.Text;


                Boolean uspelo = false;
                if (tid == tnew)
                {
                    uspelo = t_dao.newTermin(t);
                }
                else
                {
                    uspelo = t_dao.editTermin(t);
                }
                if (uspelo)
                {
                    UstanovaDetaljno uw = new UstanovaDetaljno(this.ust);
                    uw.Show();
                    this.Close();
                }


            }
            catch
            {

            }
        }

        private void cancel(object sender, RoutedEventArgs e)
        {
            this.Close();

        }
    }
}
