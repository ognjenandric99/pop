﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using SF53_2018_POP2019.Model;
using SF53_2018_POP2019.Model.Dao;

namespace SF53_2018_POP2019
{
    /// <summary>
    /// Interaction logic for editAddUcionica.xaml
    /// </summary>
    public partial class editAddUcionica : Window
    {
        UcionicaDAO uc_dao = new UcionicaDAO();
        private Ustanova ust;
        public editAddUcionica(Ustanova ust)
        {
            InitializeComponent();
            this.ust = ust;
            Ucionica_ID.Text = uc_dao.nextId().ToString();
        }
        public editAddUcionica(Ustanova ust,Ucionica uc)
        {
            InitializeComponent();
            this.ust = ust;
            Ucionica_ID.Text = uc.Id.ToString();
            Ucionica_Oznaka.Text = uc.Oznaka;
            Ucionica_Broj.Text = uc.BrojMesta.ToString();
            Ucionica_Tip.Text = uc.TipUcionice;
        }
        private void cancel(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void save(object sender, RoutedEventArgs e)
        {
            try
            {
                if (Ucionica_ID.Text.Length > 0 && Ucionica_Oznaka.Text.Length > 0 && Ucionica_Broj.Text.Length > 0 && Ucionica_Tip.Text.Length > 0)
                {
                    int uc_id = int.Parse(Ucionica_ID.Text);
                    int sl_id = uc_dao.nextId();
                    Ucionica uc = new Ucionica();
                    uc.Id = uc_id;
                    uc.Oznaka = Ucionica_Oznaka.Text;
                    uc.Ustanova = this.ust._Id.ToString();
                    uc.BrojMesta = int.Parse(Ucionica_Broj.Text);
                    uc.TipUcionice = Ucionica_Tip.Text;
                    Boolean uspelo = false;
                    if (uc_id == sl_id)
                    {
                        uspelo = uc_dao.newUcionica(this.ust, uc);
                        
                    }
                    else
                    {
                        uspelo = uc_dao.editUcionica(this.ust, uc);
                        
                    }
                    if (uspelo)
                    {
                        Ucionice uc1 = new Ucionice(this.ust);
                        uc1.Show();
                        this.Close();
                    }
                }
                else
                {

                }
            }
            catch
            {
                Console.WriteLine("Greskaaaa");
            }
            

        }
    }
}
