﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using SF53_2018_POP2019.Model;
using SF53_2018_POP2019.Model.Dao;
using SF53_2018_POP2019.Util;

namespace SF53_2018_POP2019
{
    /// <summary>
    /// Interaction logic for userWindow.xaml
    /// </summary>
    public partial class userWindow : Window
    {
        private Object _user;

        public Object user
        {
            get { return _user; }
            set { _user = value; }
        }

        public userWindow()
        {
            InitializeComponent();
            InitializeView();
            UsersDAO userdao = new UsersDAO();
            ID_Element.Text = userdao.newId().ToString();
            new_account_text.Text = "New Account";
        }
        public userWindow(Asistent user)
        {
            
            Console.WriteLine(typeof(User));
            InitializeComponent();
            InitializeView();
            this.user = user;
            new_account_text.Text = "Old Account";
            AsistentDAO userdao = new AsistentDAO();
            ID_Element.Text = user._Id.ToString();
            usernametxt.Text = user.Username;
            passwordtxt.Text = user.Password;
            imetxt.Text = user.Ime;
            prezimetxt.Text = user.Prezime;
            emailtxt.Text = user.Email;
            tipCombo.SelectedItem = TypeUser.TA;
            statusCheckBox.IsChecked = user.Status;
            comboDependTextBox.Text = user.Profesor;
        }
        public userWindow(Profesor user)
        {
            InitializeComponent();
            InitializeView();
            this.user = user;
            new_account_text.Text = "Old Account";
            ID_Element.Text = user._Id.ToString();
            usernametxt.Text = user.Username;
            passwordtxt.Text = user.Password;
            imetxt.Text = user.Ime;
            prezimetxt.Text = user.Prezime;
            emailtxt.Text = user.Email;
            tipCombo.SelectedItem = user.Tip;
            statusCheckBox.IsChecked = user.Status;
            comboDependTextBox.Text = String.Join(",", user.Asistenti.ToArray());
        }
        public userWindow(User user)
        {
            InitializeComponent();
            InitializeView();
            new_account_text.Text = "Old Account";
            usernametxt.Text = user.Username;
            passwordtxt.Text = user.Password;
            imetxt.Text = user.Ime;
            prezimetxt.Text = user.Prezime;
            emailtxt.Text = user.Email;
            tipCombo.SelectedItem = user.Tip;
            statusCheckBox.IsChecked = user.Status;
        }
        public userWindow(Administrator user)
        {
            
            InitializeComponent();
            InitializeView();
            
            Console.WriteLine("Ucitan je user sa imenom " + user.Username);
            new_account_text.Text = "Old Account";
            ID_Element.Text = user._Id.ToString();
            usernametxt.Text = user.Username;
            passwordtxt.Text = user.Password;
            imetxt.Text = user.Ime;
            prezimetxt.Text = user.Prezime;
            emailtxt.Text = user.Email;
            tipCombo.SelectedItem = user.Tip;
            statusCheckBox.IsChecked = user.Status;
            comboDependTextBox.Text = null;
        }

        public void InitializeView()
        {
            comboDependLabel.Visibility = Visibility.Hidden;
            comboDependTextBox.Visibility = Visibility.Hidden;
            foreach (var tip in TypeUser.GetValues(typeof(TypeUser)))
                tipCombo.Items.Add(tip);
            tipCombo.SelectedIndex = 0;

        }
        private void sacuvajUsera(object sender, RoutedEventArgs e)
        {
            UsersDAO userdao = new UsersDAO();
            Console.WriteLine("LINIJA : " + ID_Element.Text);
            int id = int.Parse(ID_Element.Text);
            int idSledeci = userdao.newId();
            String username = usernametxt.Text;
            String password = passwordtxt.Text;
            String ime = imetxt.Text;
            String prezime = prezimetxt.Text;
            String email = emailtxt.Text;
            var tip = tipCombo.SelectedItem;
            Boolean status = (bool)statusCheckBox.IsChecked;

            if (tip.ToString().Equals("Admin"))
            {
                Administrator admin = new Administrator(id,username,password,ime,prezime,email,status,TypeUser.Admin);
              

                AdminDAO admindao = new AdminDAO();
                Boolean x = false;
                if (id == idSledeci)
                {
                    x = admindao.newAdmin(admin);
                }
                else
                {
                    x = admindao.saveEdit(admin);
                }
                if (x)
                {
                    KorisniciWindow kw = new KorisniciWindow();
                    kw.Show();
                    this.Close();
                }
            }
            else if (tip.ToString().Equals("TA"))
            {
                Asistent asistent = new Asistent(id,username,password,ime,prezime,email,status,TypeUser.TA,comboDependTextBox.Text);
                
                AsistentDAO asistentdao = new AsistentDAO();
                Boolean x = false;
                if (id == idSledeci)
                {
                    x = asistentdao.newAsistent(asistent);
                }
                else
                {
                    x = asistentdao.saveEdit(asistent);
                }
                if (x)
                {
                    KorisniciWindow kw = new KorisniciWindow();
                    kw.Show();
                    this.Close();
                }
            }
            else if (tip.ToString().Equals("Profesor"))
            {
                String unetaVrednostPovezanosti = comboDependTextBox.Text;
                List<String> povezanost = new List<String>();
                if(unetaVrednostPovezanosti!=null && unetaVrednostPovezanosti.Length != 0)
                {
                    String[] vrednosti = unetaVrednostPovezanosti.Split(',');
                    foreach (String item in vrednosti)
                    {
                        povezanost.Add(item);
                    }
                }
                Profesor profesor = new Profesor(id,username,password,ime,prezime,email,status,TypeUser.Profesor,povezanost);

                ProfesorDAO profadao = new ProfesorDAO();
                Boolean x = false;
                if (id == idSledeci)
                {
                    x = profadao.newProfesor(profesor);
                }
                else
                {
                    x = profadao.saveEdit(profesor);
                    Console.WriteLine("ASDASD");
                }
                if (x)
                {
                    KorisniciWindow kw = new KorisniciWindow();
                    kw.Show();
                    this.Close();
                }
                else
                {
                    Console.WriteLine("Desila se greska");
                }
            }
        }

        private void cancel(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void comboBoxTypeUserChange()
        {
            var tip = tipCombo.SelectedItem;
            if (tip.Equals(TypeUser.TA))
            {
                comboDependLabel.Content = "Username profesora : ";
                comboDependLabel.Visibility = Visibility.Visible;
                comboDependTextBox.Visibility = Visibility.Visible;
            }
            else if (tip.Equals(TypeUser.Profesor))
            {
                comboDependLabel.Content = "Usernamovi Asistenata (Odvojte zarezom) : ";
                comboDependLabel.Visibility = Visibility.Visible;
                comboDependTextBox.Visibility = Visibility.Visible;
            }
            else
            {
                comboDependLabel.Visibility = Visibility.Hidden;
                comboDependTextBox.Visibility = Visibility.Hidden;
            }
        }


        private void tipCombo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            comboBoxTypeUserChange();
        }
        public void prikaziError(String error)
        {
            
        }
    }
}
