﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using SF53_2018_POP2019.window_neregistrovan;
using SF53_2018_POP2019.Model;
using SF53_2018_POP2019.Model.Dao;

namespace SF53_2018_POP2019.window_neregistrovan
{
    /// <summary>
    /// Interaction logic for Ustanove.xaml
    /// </summary>
    public partial class Ustanove : Window
    {
        public Ustanove()
        {
            InitializeComponent();
            UstanovaDAO udao = new UstanovaDAO();
            ucitajUTabelu(udao.selectUstanove());
        }

        private void otvoriUstanovu1(object sender, RoutedEventArgs e)
        {

        }
        private void ucitajUTabelu(List<Ustanova> listaUstanova)
        {

            tableUstanove.ItemsSource = CollectionViewSource.GetDefaultView(listaUstanova);
            tableUstanove.CanUserAddRows = false;
        }
        private void cancel1(object sender, RoutedEventArgs e)
        {
            glavniProzor gp = new glavniProzor();
            gp.Show();
            this.Close();
        }
    }
}
