﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF53_2018_POP2019.window_neregistrovan
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class glavniProzor : Window
    {
        public glavniProzor()
        {
            InitializeComponent();
        }

        private void nereg_main_ustanove(object sender, RoutedEventArgs e)
        {
            Ustanove ust = new Ustanove();
            ust.Show();
            this.Close();
        }

        private void nereg_main_nazad(object sender,RoutedEventArgs e)
        {
            Login loginWindow = new Login();
            loginWindow.Show();
            this.Close();
        }
    }
}
